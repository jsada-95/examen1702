<%@page import="model.Post"%>
<%@include file="/WEB-INF/view/header.jsp" %>
<jsp:useBean id="post" class="model.Post" scope="request"/>  

<div id="content">
    <% if(post.getId() == 0){%>
    <h1> Registro no encontrado </h1>
    <% } else { %>
    <h1>Vista de post </h1>
   
    <p>Id: <%= post.getId()%></p><br>
    <p>Author: <%= post.getAuthor()%></p><br>
    <p>Titulo: <%= post.getTitle()%></p><br>
    <p>Content: <%= post.getContent()%></p><br>

    <% } %>
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
