
<%@page import="model.Post"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="posts" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de Post</h1>
    <p>
        <a href="<%= request.getContextPath()%>/post/create">Nuevo post</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>Autor</th>
            <th>Titulo</th> 
        </tr>
        <%        Iterator<model.Post> iterator = posts.iterator();
            while (iterator.hasNext()) {
                Post post = iterator.next();%>
        <tr>
            <td><%= post.getId()%></td>
            <td><%= post.getAuthor()%></td>
            <td><%= post.getTitle()%></td>
 
            <td> 
                <a href="<%= request.getContextPath() + "/post/view/" + post.getId()%>"> Ver </a>
            </td>
        </tr>
        <%
            }
        %>          
    </table>
    <a href="<%= request.getContextPath() + "/post/index/1" %>"> 1</a>
    <a href="<%= request.getContextPath() + "/post/index/2" %>"> 2</a>
    <a href="<%= request.getContextPath() + "/post/index/3" %>"> 3</a>
    <a href="<%= request.getContextPath() + "/post/index/4" %>"> 4</a>
    <a href="<%= request.getContextPath() + "/post/index/5" %>"> 5</a>
    <a href="<%= request.getContextPath() + "/post/index/6" %>"> 6</a>
    <br>

   
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
