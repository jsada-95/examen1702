/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import static java.lang.Long.parseLong;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import model.Post;
import persistence.PostDAO;

/**
 *
 * @author usuario
 */
public class PostController extends BaseController {
    
    private static final int ELEMENTS_PER_PAGE = 20;
    private static final Logger LOG = Logger.getLogger(PostController.class.getName());
    private PostDAO postDAO;

    public void index(String page) {
        
        int pageNum = 1;
        //objeto persistencia
        postDAO = new PostDAO();
        ArrayList<Post> posts = null;
        
        pageNum = Integer.parseInt(page);
         
        //leer datos de la persistencia
        synchronized (postDAO) {
            posts = postDAO.getAll(pageNum, ELEMENTS_PER_PAGE);
        } //para que no haya 2 conexiones a la vez
        request.setAttribute("posts", posts);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/post/index.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/post/create.jsp");
    }

    public void store() throws IOException {

        //objeto persistencia
        postDAO = new PostDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Post post = loadFromRequest();
        
        synchronized (postDAO) {
            try {
                postDAO.store(post);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/post/index/1");
    }
    
    private Post loadFromRequest()
    {
        Post post = new Post();
        LOG.info("Crear modelo");
        
        post.setId(toId(request.getParameter("id")));
        post.setAuthor(request.getParameter("author"));
        post.setTitle(request.getParameter("title"));
        post.setContent(request.getParameter("content"));
        
        LOG.info("Datos cargados");
        return post;
    }
    public void view(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        PostDAO  postDAO = new PostDAO();
        Post post = postDAO.get(id);
        request.setAttribute("post", post);
       
        this.guardarSesion(idString);
        dispatch("/WEB-INF/view/post/show.jsp");

    }
    public void guardarSesion(String idString) throws SQLException {
        HttpSession session = request.getSession(true);
        session.setAttribute("id", idString); 
    }
    public void last(String idString) throws SQLException {
        redirect(contextPath + "/post/view/" + idString);
    }
}
