/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import model.Post;

/**
 *
 * @author usuario
 */
public class PostDAO extends BaseDAO{
    public PostDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }
    public ArrayList<Post> getAll(int page, int elementsPerPage) {
        PreparedStatement stmt = null;
        ArrayList<Post> posts = null;
        int offset = (page - 1) * elementsPerPage;
        
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from posts LIMIT " + elementsPerPage + " OFFSET " + offset);
            ResultSet rs = stmt.executeQuery();
            posts = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Post post = new Post();
                post.setId(rs.getLong("id"));
                post.setAuthor(rs.getString("author"));
                post.setTitle(rs.getString("title"));          

                posts.add(post);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return posts;
    }
    public Post get(long id) throws SQLException {
        LOG.info("get(id)");
        Post post = new Post();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM posts"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            post.setId(rs.getLong("id"));
            post.setAuthor(rs.getString("author"));
            post.setTitle(rs.getString("title"));
            post.setContent(rs.getString("content"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return post;
    }
   
    public void store(Post post) throws SQLException {
        PreparedStatement stmt = null;
        LOG.info("Crear DAO");
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO posts(author, title, content)"
                + " VALUES(?, ?, ?)"
        );
        //stmt.setLong(1, 4); // teacher.getId()
        
        //LOG.info(""+teacher.getId());
        stmt.setString(1, post.getAuthor());
        stmt.setString(2, post.getTitle());
        stmt.setString(3, post.getContent());

        stmt.execute();
        this.disconnect();
    }
  
    
}
